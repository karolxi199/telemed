const mongoose = require('mongoose');
const { Schema } = mongoose;

const ImgSchema = new Schema({
    url: {
        type: String,
    }

});

module.exports = mongoose.model('Img', ImgSchema);