const mongoose = require('mongoose');
const { Schema } = mongoose;

const EspecialidadSchema = new Schema({
    doc: {
        type: String,
        required: true
    },
    espec: {
        type: String,
        required: true
    },
    titulo: {
        type: String,
        required: true
    },
    estado: {
        type: String,
        required: true
    },

    user: {
        type: String
    },
    date: {
        type: Date,
        default: Date.now
    },
    url: {
        type: String,
    }

});

module.exports = mongoose.model('Especialidad', EspecialidadSchema);