const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;

const User = require('../models/User');


// Serializado Local
passport.serializeUser((user, call) => {
    call(null, user.id);
});

passport.deserializeUser((id, call) => {
    User.findById(id, (err, user) => {
        call(err, user);
    });
});


// Passport con usuario local
passport.use(new LocalStrategy({
    usernameField: 'email'
}, async(email, password, call) => {
    const user = await User.findOne({ email: email });
    if (!user) {
        return call(null, false, {
            message: 'No se encontró el usuario'
        });
    } else {
        const match = await user.matchPassword(password);
        if (match) {
            return call(null, user);
        } else {
            return call(null, false, {
                message: 'Contraseña incoreccta'
            });
        }
    }
}));