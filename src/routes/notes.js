const express = require('express');
const router = express.Router();

const { isAuthenticated } = require('../helpers/auth');

// Requerir model back
const Note = require('../models/Note');
const Competencias = require('../models/Competencias');
const Formulario = require('../models/Formulario');

const Especialidad = require('../models/Especialidad');

router.get('/notes/add', isAuthenticated, (req, res) => {
    res.render('notes/new-note');
});
router.get('/notes/participantes', isAuthenticated, async(req, res) => {

    const form = await Formulario.find();

    res.render('notes/participantes', { form });
});




// Rutas de back

router.post('/notes/new-note', async(req, res) => {
    const { espec, doc, titulo, estado, url } = req.body;
    const errors = [];

    if (!espec) {
        errors.push({
            text: 'Por favor insertar una Especialidad'
        });
    }
    if (!doc) {
        errors.push({
            text: 'Por favor insertar el nombre'
        });
    }
    if (!estado) {
        errors.push({
            text: 'Por favor insertar el estado'
        });
    }
    if (!url) {
        errors.push({
            text: 'Por favor insertar una url de la imagen'
        });
    }
    if (!titulo) {
        errors.push({
            text: 'Por favor insertar la profesión'
        });
    }
    if (errors.length > 0) {
        res.render('notes/new-note', {
            errors,
            espec,
            doc,
            estado,
            titulo,
            url,

        });
    } else {
        const newNotes = new Especialidad({
            espec,
            doc,
            estado,
            titulo,
            url,

        });
        // Esta line me permite saber el enlace de la nota con el user
        newNotes.user = req.user.id;
        await newNotes.save();
        req.flash('success_msg', 'Se registro la especialidad');
        res.redirect('/notes');
    }
});

// Esta ruta va a recibir notas de la db
router.get('/notes', isAuthenticated, async(req, res) => {
    const notes = await Especialidad.find({ user: req.user.id }).sort({ date: 'desc' });

    // Con esto va a renderizar la pag y pasarle las notas
    // res.render('notes/all-notes');
    res.render('notes/all-notes', { notes });
});


// Esta ruta te permite traer la vista para editar, debe llamar a un formulario

router.get('/notes/edit/:id', isAuthenticated, async(req, res) => {
    const note = await Especialidad.findById(req.params.id);
    res.render('notes/edit-note', { note });
});
router.get('/notes/editP/:id', isAuthenticated, async(req, res) => {
    const note = await Formulario.findById(req.params.id);
    res.render('notes/edit-par', { note });
});

// Esta ruta hace la peticion put para actualizar la db

router.put('/notes/edit-noteP/:id', isAuthenticated, async(req, res) => {
    const { estado, fecha, hora } = req.body;
    await Formulario.findByIdAndUpdate(req.params.id, { estado, fecha, hora });
    req.flash('success_msg', 'La especialidad se actualizó correctamente');
    res.redirect('/notes/participantes');
});
router.put('/notes/edit-note/:id', isAuthenticated, async(req, res) => {
    const { espec, doc, titulo, estado, url } = req.body;
    await Especialidad.findByIdAndUpdate(req.params.id, { espec, doc, titulo, estado, url });
    req.flash('success_msg', 'La especialidad se actualizó correctamente');
    res.redirect('/notes');
});

// para eliminar la nota

router.delete('/notes/delete/:id', isAuthenticated, async(req, res) => {
    await Especialidad.findByIdAndDelete(req.params.id);
    req.flash('success_msg', 'La especialidad se eliminó correctamente');
    res.redirect('/notes');
});

module.exports = router;