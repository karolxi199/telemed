const express = require('express');
const router = express.Router();
const User = require('../models/User');
const passport = require('passport');
const session = require('express-session');


const { isAuthenticated } = require('../helpers/auth');
const Competencias = require('../models/Competencias');
const Formulario = require('../models/Formulario');

const Especialidad = require('../models/Especialidad');


router.get('/users/signin', (req, res) => {
    res.render('./users/signin');
});
router.get('/users/formulario', async(req, res) => {
    const notes = await Especialidad.find();

    res.render('./users/formulario', { notes });
});

// Autenticar Usuario
router.post('/users/signin', passport.authenticate('local', {
    successRedirect: '/notes',
    failureRedirect: '/users/signin',
    failureFlash: 'Por favor ingresar sus credenciales!'
}));

router.get('/users/signup', (req, res) => {
    res.render('./users/signup');
});

router.post('/users/formulario', async(req, res) => {
    const { name, iden, email, espec, doc, fecha, hora, estado } = req.body;
    const errors = [];

    if (name == '' || email == '' || iden == '' || espec == '' || doc == '' || fecha == '' || hora == '') {
        errors.push({
            text: 'Ingrese todos los campos'
        });
    }

    if (errors.length > 0) {
        res.render('users/formulario', {
            errors,
            // name,
            // password,
            // email,
            // confirm_password
        });

    } else {
        const newUser = new Formulario({
            name,
            iden,
            email,
            espec,
            doc,
            fecha,
            hora,
            estado
        });
        await newUser.save();
        req.flash('success_msg', 'Su cita se registró correctamente, espere su notificación al correo');
        res.redirect('/');
    }

});

router.post('/users/signup', async(req, res) => {
    const { name, email, password, confirm_password } = req.body;
    const errors = [];

    if (name == '' || email == '' || password == '' || confirm_password == '') {
        errors.push({
            text: 'Ingrese todos los campos'
        });
    }
    if (password != confirm_password) {
        errors.push({
            text: 'Las contraseñas no son iguales'
        });
    }
    if (password.length < 4) {
        errors.push({
            text: 'Las contraseñas debe tener mas de 4 caracteres'
        });
    }
    const emailUser = await User.findOne({ email: email });
    if (emailUser) {
        errors.push({
            text: 'El correo ingresado ya esta en uso'
        });
    }
    if (errors.length > 0) {
        res.render('users/signup', {
            errors,
            // name,
            // password,
            // email,
            // confirm_password
        });

    } else {
        const newUser = new User({
            name,
            email,
            password
        });
        newUser.password = await newUser.encryptPassword(password);
        await newUser.save();
        req.flash('success_msg', 'El usuario se registro corectamente');
        res.redirect('/users/signin');
    }

});

// Cerrar sesion

router.get('/users/logout', isAuthenticated, (req, res) => {
    req.logout();
    res.redirect('/');
});


module.exports = router;